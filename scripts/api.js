import http from "k6/http";
import { check } from "k6";

const BASE_URL = __ENV.BASE_URL || 'https://quickpizza.eyeveebee.net';

export const options = {
    vus: 1,
    iterations: 5,
    thresholds: {
        http_req_duration: ['p(95)<1000'], // 95 percent of response times must be below 1s
    },
};

export function setup() {
    let res = http.get(BASE_URL);
    if (res.status !== 200) {
        throw new Error(`Got unexpected status code ${res.status} when trying to setup. Exiting.`)
    }
}

export default function () {
    let restrictions = {
        maxCaloriesPerSlice: 500,
        mustBeVegetarian: false,
        excludedIngredients: ["pepperoni"],
        excludedTools: [],
        maxNumberOfToppings: 6,
        minNumberOfToppings: 2,
    }
    let res = http.post(`${BASE_URL}/api/pizza`, JSON.stringify(restrictions), {
        headers: {
            'Content-Type': 'application/json',
            'X-User-ID': 12351,
        },
    });
    check(res, { "status is 200": (res) => res.status === 200 });
    console.log(`${res.json().pizza.name} (${res.json().pizza.ingredients.length} ingredients)`);
}