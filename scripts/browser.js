import { browser } from 'k6/experimental/browser';
import { check } from 'k6';

const BASE_URL = __ENV.BASE_URL || 'https://quickpizza.eyeveebee.net';

export const options = {
    scenarios: {
        ui: {
            executor: 'shared-iterations',
            options: {
                browser: {
                    type: 'chromium',
                },
            },
        },
    },
    thresholds: {
        checks: ["rate==1.0"]
    }
}

export default async function () {
    const context = browser.newContext();
    const page = context.newPage();

    try {
        await page.goto(BASE_URL);
        check(page, {
            'header': page.locator('h1').textContent() == 'Looking to break out of your pizza routine?',
        });
        await page.locator('//button[. = "Pizza, Please!"]').click();
        page.waitForTimeout(1000);
        check(page, {
            'recommendation': page.locator('div#recommendations').textContent() != '',
        });
    } finally {
        page.close();
    }
}